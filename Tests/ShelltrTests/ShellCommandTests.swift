//
//  ShellCommandTests.swift
//  ShelltrTests
//
//  Created by Cavelle Benjamin on 19-Feb-17 (07).
//

import Shelltr
import XCTest

@available(macOS 10.13, *)
class ShellCommandTests: XCTestCase {
  override func setUp() {
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }

  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
  }

  func testEchoShellCommand() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    let command = ShellCommand(named: "echo")

    XCTAssertEqual(command.name, "echo")
    XCTAssertEqual(command.arguments, [String]())
    XCTAssertEqual(command.environment, nil)
    XCTAssertEqual(command.emoji, "🦄")
    XCTAssertEqual(command.directory, Shell.cwd)
  }
}
