import Shelltr
import XCTest

@available(macOS 10.13, *)
final class ShelltrTests: XCTestCase {
  override static func setUp() {
    Shell.debug = true
  }

  func testRawEchoHelloWorld() {
    do {
      let shell = Shell(debug: true)

      let results = try shell.execute(
        "/bin/bash",
        arguments: ["-l", "-c", "echo 'Hello, world!'"],
        environment: nil,
        emoji: "😂",
        at: "."
      )

      print(results)

      XCTAssertEqual(results.output, "Hello, world!")
      XCTAssertEqual(results.error, "")
      XCTAssertEqual(results.status, Int32(0))

    } catch {
      print(error)
      XCTFail("Failure occured: \(error.localizedDescription)")
    }
  }

  func testLookupCommand() {
    do {
      let shell = Shell(.bash, debug: true)

      let command = "echo"

      let results = try shell.lookup(command)

      print(results)

      XCTAssertEqual(results.output, "/bin/echo")
      XCTAssertEqual(results.error, "")
      XCTAssertEqual(results.status, Int32(0))

    } catch {
      print(error)
      XCTFail("Failure occured: \(error.localizedDescription)")
    }
  }

  func testLookupEcho() {
    do {
      let shell = Shell(.bash, debug: true)

      let command = "echo"

      let results = try shell.lookup(command)

      print(results)

      XCTAssertEqual(results.output, "/bin/echo")
      XCTAssertEqual(results.error, "")
      XCTAssertEqual(results.status, Int32(0))

    } catch {
      print(error)
      XCTFail("Failure occured: \(error.localizedDescription)")
    }
  }

  func testShellCommandEcho() {
    do {
      let shell = Shell(.bash, debug: true)

      let command = ShellCommand(named: "echo", arguments: ["Hello, world!"])

      let results = try shell.execute(command)

      print(results)

      XCTAssertEqual(results.output, "Hello, world!")
      XCTAssertEqual(results.error, "")
      XCTAssertEqual(results.status, Int32(0))

    } catch {
      print(error)
      XCTFail("Failure occured: \(error.localizedDescription)")
    }
  }

  func testShellCommandEchoExtension() {
    do {
      let shell = Shell(.bash, debug: true)

      let results = try shell.execute(.echo(["Hello, world!"]))

      print(results)

      XCTAssertEqual(results.output, "Hello, world!")
      XCTAssertEqual(results.error, "")
      XCTAssertEqual(results.status, Int32(0))

    } catch {
      print(error)
      XCTFail("Failure occured: \(error.localizedDescription)")
    }
  }

  func testShellFunctionEchoExtension() {
    do {
      Shell.debug = false

      try echo("Hello, world! (shell function)")

    } catch {
      print(error)
      XCTFail("Failure occured: \(error.localizedDescription)")
    }
  }

  func testShellWork() {
    Shell.work(debug: true) {
      try Shell.echo("Hello, world! (shell function)")
    }
  }

  func testShellWait() {
    Shell.work(debug: true) {
      try Shell.wait(10, for: "google.com", on: 443)
      try Shell.echo("Hello, world!")
    }
  }

  static var allTests = [
    ("testRawEchoHelloWorld", testRawEchoHelloWorld),
    ("testLookupCommand", testLookupCommand),
    ("testLookupEcho", testLookupEcho),
    ("testShellCommandEcho", testShellCommandEcho),
    ("testShellCommandEchoExtension", testShellCommandEchoExtension),
    ("testShellWork", testShellWork),
    ("testShellWait", testShellWait)
  ]
}
