import XCTest

import ShelltrTests

var tests = [XCTestCaseEntry]()
tests += ShelltrTests.allTests()
XCTMain(tests)
