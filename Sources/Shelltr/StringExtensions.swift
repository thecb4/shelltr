//
//  StringExtensions.swift
//  Shelltr
//
//  Created by Cavelle Benjamin on 19-Feb-18 (08).
//

import Foundation

extension String {
  public var isAbsolute: Bool {
    if let first = self.first, first == "/" {
      return true
    } else {
      return false
    }
  }
}
