//
//  ShellError.swift
//  Shelltr
//
//  Created by Cavelle Benjamin on 19-Feb-16 (07).
//

import Foundation

enum ShellError: Error {
  case executionError(String, String, Int32)
}
