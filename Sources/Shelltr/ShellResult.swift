//
//  ShellResult.swift
//  Shelltr
//
//  Created by Cavelle Benjamin on 19-Feb-17 (07).
//

import Foundation

// https://github.com/JohnSundell/ShellOut/blob/master/Sources/ShellOut.swift
extension Data {
  func shellOutput() -> String {
    guard let output = String(data: self, encoding: .utf8) else {
      return ""
    }

    // guard !output.hasSuffix("\n") else {
    //   let endIndex = output.index(before: output.endIndex)
    //   return String(output[..<endIndex])
    // }

    return output
  }
}

public struct ShellResult {
  public let output: String
  public let error: String
  public let status: Int32

  public func printOutput() {
    print("\(output)")
  }

  public func printError() {
    print("\(error)")
  }

  public func printStatus() {
    print("\(status)")
  }
}
