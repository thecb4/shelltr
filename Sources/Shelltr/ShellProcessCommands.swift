//
//  ShellProcessCommands.swift
//  Shelltr
//
//  Created by Cavelle Benjamin on 19-Feb-17 (07).
//

import Foundation

public enum GitAddMode {
  case all
  case newModified
  case modifiedDeleted
}

@available(macOS 10.13, *)
extension Shell {
  public static func echo(shellType: ShellType = .bash, _ words: @autoclosure () -> String) throws {
    try Shell(shellType).execute(.echo([words()]))
  }

  public static func swiftformat(shellType: ShellType = .bash, arguments: [String] = [], sources: [String] = ["."]) throws {
    let args = arguments + sources
    try Shell(shellType).execute(.swiftformat(args))
  }

  public static func swiftlint(shellType: ShellType = .bash, quiet: Bool = true) throws {
    var args = ["lint"]
    if quiet { args += ["--quiet"] }
    try Shell(shellType).execute(.swiftlint(args)).printOutput()
  }

  public static func swiftbuild(shellType: ShellType = .bash, arguments: [String] = []) throws {
    let args = ["build"] + arguments
    try Shell(shellType).execute(.swift(args))
  }

  public static func swifttest(shellType: ShellType = .bash, arguments: [String] = []) throws {
    let args = ["test"] + arguments
    try Shell(shellType).execute(.swift(args))
  }

  public static func sourcekitten(shellType: ShellType = .bash, module: String, destination: String) throws {
    let args = ["doc", "--spm-module", module]
    let result = try Shell(shellType).execute(.sourcekitten(args), reportOut: false)

    var destinationString = ""

    if destination.isAbsolute {
      destinationString = destination
    } else {
      destinationString = Shell.cwd + "/\(destination)"
    }
    let destinationPath = URL(fileURLWithPath: destinationString)
    try result.output.write(to: destinationPath, atomically: false, encoding: .utf8)
  }

  public static func jazzy(shellType: ShellType = .bash) throws {
    let args = [String]()
    try Shell(shellType).execute(.jazzy(args))
  }

  public static func rm(shellType: ShellType = .bash, content: @autoclosure () -> String, recursive: Bool = false, force: Bool = false) throws {
    var args = [String]()

    if recursive {
      args += ["-r"]
    }

    if force {
      args += ["-f"]
    }

    args += [content()]

    try Shell(shellType).execute(.rm(args))
  }

  public static func copy(shellType: ShellType = .bash, source: String, destination: String) throws {
    let args = [source, destination]
    try Shell(shellType).execute(.copy(args))
  }

  public static func gitLS(shellType: ShellType = .bash, arguments: [String]) throws -> [String] {
    let args = ["ls-files"] + arguments
    return try Shell(shellType).execute(.git(args), reportOut: false).output.components(separatedBy: "\n")
  }

  public static func assertModified(shellType: ShellType = .bash, file: String) throws {
    let list = try gitLS(shellType: shellType, arguments: ["--exclude-standard", "--others", "--modified"])
    if !list.contains(file) {
      print("🤮 | I expect you to modify [\(file)] before committing.")
      throw ShellError.executionError("", "🤮 | I expect you to modify [\(file)] before committing.", Int32(1))
    }
  }

  public static func gitAdd(shellType: ShellType = .bash, _ mode: GitAddMode) throws {
    let args: [String]

    switch mode {
      case .all: args = ["add", "."]
      case .newModified: args = ["add", "--ignore-removed"]
      case .modifiedDeleted: args = ["add", "--ignore-removed"]
    }

    try Shell(shellType).execute(.git(args))
  }

  public static func gitCommit(shellType: ShellType = .bash, message: @autoclosure () -> String) throws {
    let args = ["commit", "-m", message()]
    try Shell(shellType).execute(.git(args))
  }

  public static func gitRevealSecrets(shellType: ShellType = .bash, force: Bool = false, preserve: Bool = false, password: String = "secret") throws {
    var args = ["secret", "reveal"]

    if force {
      args += ["-f"]
    }

    if preserve {
      args += ["-p"]
    }

    args += [password]

    try Shell(shellType).execute(.git(args))
  }

  public static func gitHideSecrets(shellType: ShellType = .bash, delete: Bool = false) throws {
    var args = ["secret", "hide"]

    if delete {
      args += ["-d"]
    }

    try Shell(shellType).execute(.git(args))
  }

  public static func dockerComposeUp(shellType: ShellType = .bash, detached: Bool = false, files: [String]? = nil, services: [String]? = nil, at directory: String = Shell.cwd) throws {
    var args = [String]()

    if let files = files {
      for file in files {
        args += ["-f", file]
      }
    }

    args += ["up"]

    if detached {
      args += ["-d"]
    }

    if let services = services {
      args += services
    }

    try Shell(shellType).execute(.dockerCompose(args, echoString: "starting docker services: \(String(describing: services))", directory: directory))
  }

  public static func dockerComposeDown(shellType: ShellType = .bash, files: [String]? = nil, at directory: String = Shell.cwd) throws {
    var args = [String]()

    if let files = files {
      for file in files {
        args += ["-f", file]
      }
    }

    args += ["down"]

    try Shell(shellType).execute(.dockerCompose(args, echoString: "stopping docker services", directory: directory))
  }

  public static func wait(shellType: ShellType = .bash, _ seconds: TimeInterval, for host: String, on port: Int) throws {
    print("⏱ | waiting at most \(seconds) seconds for \(host):\(port)...")

    let args = ["-l", "-c", "exec 3<> /dev/tcp/\(host)/\(port);echo $?", "2>/dev/null"]

    var results: ShellResult

    let start = Date()

    repeat {
      results = try Shell(shellType).execute(.shell(named: shellType.name, args))
      if results.output != "0" {
        print("😢 | Couldn't connect, trying again in 5 seconds")
      }
      sleep(5)
    } while results.output != "0" && abs(start.timeIntervalSinceNow) < seconds

    if results.output != "0" {
      print("💩 | Couldn't connect, timed out after \(seconds) seconds")
    } else {
      print("🦄 | Service running on \(host):\(port)")
    }
  }

  public static func xcodegenGenerate(shellType: ShellType = .bash) throws {
    let args = ["generate"]
    try Shell(shellType).execute(.xcodegen(args))
  }

  public static func xcodebuild(shellType: ShellType = .bash, arguments: [String] = []) throws {
    try Shell(shellType).execute(.xcodebuild(arguments))
  }

  public static func bumpBuild(shellType: ShellType = .bash, to buildNumber: Int? = nil) throws {
    var args = [String]()

    if let build = buildNumber {
      args += ["new-version", "-all", "\(build)"]
    } else {
      args += ["next-version", "-all"]
    }

    try Shell(shellType).execute(.agvtool(args))
  }

  public static func gitCurrentBranch(shellType: ShellType = .bash) throws -> String {
    let args = ["rev-parse", "--abbrev-ref", "HEAD"]
    return try Shell(shellType).execute(.git(args), reportOut: false).output
  }

  public static func currentBranchType(shellType: ShellType = .bash) throws -> String {
    let output = try Shell.gitCurrentBranch(shellType: shellType)

    if let type = output.components(separatedBy: "/").first {
      return type
    } else {
      throw "branch not prefixed: \(String(describing: output))"
    }
  }

  public static func isGitFlowBranch(shellType: ShellType = .bash, type: String) -> Bool {
    if let _type = try? currentBranchType(shellType: shellType) {
      return _type == type
    } else {
      return false
    }
  }

  public static func gitFlowVersion(shellType: ShellType = .bash) throws -> String {
    let releaseType = ["release", "hotfix"]

    let output = try? Shell.gitCurrentBranch(shellType: shellType)

    if
      let branch = output,
      let type = branch.components(separatedBy: "/").first,
      releaseType.contains(type),
      let version = branch.components(separatedBy: "/").last {
      return version
    } else {
      throw "Cannot get git-flow-version from : \(String(describing: output))"
    }
  }

  public static func setVersion(shellType: ShellType = .bash, to version: String) throws {
    let args = ["new-marketing-version", version]

    try Shell(shellType).execute(.agvtool(args))
  }

  public static func curl(shellType: ShellType = .bash, arguments: [String] = []) throws {
    try Shell(shellType).execute(.curl(arguments))
  }

  public static func waitHTTP(shellType: ShellType = .bash, _ seconds: TimeInterval, for host: String) throws {
    print("⏱ | waiting at most \(seconds) seconds for \(host)...")

    let args = ["--insecure", "-s", "-o", "/dev/null", "-w", "%{http_code}\n", host]

    var results: ShellResult

    let start = Date()

    repeat {
      results = try Shell(shellType).execute(.curl(args))
      if results.output != "200" {
        print("😢 | Couldn't connect, trying again in 5 seconds")
      }
      sleep(5)
      // swiftformat:disable andOperator
    } while results.output != "200" && abs(start.timeIntervalSinceNow) < seconds
    // swiftformat:enable all
    if results.output != "200" {
      print("💩 | Couldn't connect, timed out after \(seconds) seconds")
      throw "Bad Connection Error: \(host)"
    } else {
      print("🦄 | HTTP Health Check Passed")
    }
  }

  // convert $FILE -resize 180x180! resized/${filename}-60x60@3x.png
  public static func image(shellType: ShellType = .bash, source: String, size: Size, destination: String) throws {
    // let env = ["PKG_CONFIG_PATH": "/usr/local/opt/imagemagick@6/lib/pkgconfig"]

    let args = [source, "-resize", "\(size.width)x\(size.height)!", destination]
    try Shell(shellType).execute(.image(args))
  }
}
