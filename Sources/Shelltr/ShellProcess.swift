import Foundation

public enum ShellType: String {
  case sh
  case bash
  case zsh

  var path: String {
    return "/bin/" + rawValue
  }

  var name: String {
    return rawValue
  }
}

@available(macOS 10.13, *)
public class Shell {
  public static let cwd = FileManager.default.currentDirectoryPath
  public static var debug = false

  var type: ShellType
  var process: Process!
  var outData: Data!
  var errData: Data!
  var outPipe: Pipe!
  var errPipe: Pipe!
  var reportOut: Bool
  var reportErr: Bool
  var emoji: String?
  var debug: Bool

  public init(_ type: ShellType = .sh, reportOut: Bool = true, reportErr: Bool = true, emoji: String? = nil, debug: Bool = Shell.debug) {
    self.type = type
    self.reportOut = reportOut
    self.reportErr = reportErr
    self.emoji = emoji
    self.debug = debug
  }

  public func handleOutput(file: FileHandle) {
    let data = file.availableData

    guard let line = String(data: data, encoding: .utf8) else { return }

    // https://stackoverflow.com/questions/27768064/check-if-swift-text-field-contains-non-whitespace
    let trimmed = line.trimmingCharacters(in: .whitespacesAndNewlines)

    if !trimmed.isEmpty {
      if let emoji = self.emoji {
        let emojied = trimmed.components(separatedBy: .newlines).map { "\(emoji) | " + $0 }.joined(separator: "\n")
        if reportOut { print(emojied) }

      } else {
        if reportOut { print(trimmed) }
      }

      guard let output = trimmed.data(using: .utf8) else { return }
      outData.append(output)
    }
  }

  public func handleError(file: FileHandle) {
    let data = file.availableData
    guard let line = String(data: data, encoding: .utf8) else { return }

    // https://stackoverflow.com/questions/27768064/check-if-swift-text-field-contains-non-whitespace
    let trimmed = line.trimmingCharacters(in: .whitespacesAndNewlines)

    if !trimmed.isEmpty {
      if let emoji = self.emoji {
        let emojied = trimmed.components(separatedBy: .newlines).map { "\(emoji) | " + $0 }.joined(separator: "\n")
        if reportErr { print(emojied) }

      } else {
        if reportErr { print(trimmed) }
      }

      guard let output = trimmed.data(using: .utf8) else { return }
      errData.append(output)
    }
  }

  public func prepare() {
    process = Process()

    outData = Data()
    errData = Data()

    outPipe = Pipe()
    errPipe = Pipe()

    process.standardOutput = outPipe
    process.standardError = errPipe

    outPipe.fileHandleForReading.readabilityHandler = handleOutput
    errPipe.fileHandleForReading.readabilityHandler = handleError
  }

  public func end() {
    outPipe.fileHandleForReading.readabilityHandler = nil
    errPipe.fileHandleForReading.readabilityHandler = nil
    outData = nil
    errData = nil
    outPipe = nil
    errPipe = nil
    process = nil
  }

  public func execute(
    _ launchPath: String,
    arguments: ShellAguments = [],
    environment: ShellEnvironment? = nil,
    emoji: String? = nil,
    at pwd: String = Shell.cwd,
    reportOut: Bool = true,
    reportErr: Bool = true
  ) throws -> ShellResult {
    self.emoji = emoji

    self.reportOut = reportOut
    self.reportErr = reportErr

    prepare()

    process.executableURL = URL(fileURLWithPath: launchPath)
    process.arguments = arguments

    if let env = environment {
      process.environment = env
    }

    process.currentDirectoryURL = URL(fileURLWithPath: pwd)

    if debug {
      print("executableURL = \(String(describing: process.executableURL))")
      print("arguments = \(String(describing: process.arguments))")
      print("env = \(String(describing: process.environment))")
      print("currentDirectoryURL = \(String(describing: process.currentDirectoryURL))")
    }

    try process.run()

    process.waitUntilExit()

    let result = ShellResult(output: outData.shellOutput(), error: errData.shellOutput(), status: process.terminationStatus)

    if debug { print(result) }

    end()

    if result.status != 0 {
      throw ShellError.executionError(result.output, result.error, result.status)
    }

    return result
  }

  public func lookup(_ command: String) throws -> ShellResult {
    let arguments = ["-l", "-c", "which \(command)"]

    return try execute(type.path, arguments: arguments, reportOut: false, reportErr: true)
  }

  @discardableResult
  public func execute(_ command: ShellCommand, reportOut: Bool = true, reportErr: Bool = true) throws -> ShellResult {
    if debug { print("looking up command = \(command.name)") }

    let path = try lookup(command.name).output

    if debug { print(command) }

    return try execute(
      path,
      arguments: command.arguments,
      environment: command.environment,
      emoji: command.emoji,
      at: command.directory,
      reportOut: reportOut,
      reportErr: reportErr
    )
  }

  public static func work(debug: Bool = false, instructions: @escaping () throws -> Void) {
    Shell.debug = debug

    let startTime = Date()

    do {
      try instructions()

    } catch let error as ShellError {
      print(error)

    } catch {
      print(error)
    }

    let endTime = Date()
    let formatter = DateComponentsFormatter()
    formatter.unitsStyle = .full
    formatter.allowedUnits = [.hour, .minute, .second]
    guard let string = formatter.string(from: startTime, to: endTime) else {
      exit(1)
    }
    // https://stackoverflow.com/questions/33780184/calculate-duration-between-date-ios-in-years-months-and-date-format
    // print(string) // prints: 4 years, 5 months, 9 days
    print("effort took \(string)")
  }
}
