
import Foundation

// public typealias ShellResult = (output: String, error: String, status: Int32)
public typealias ShellAguments = [String]
public typealias ShellEnvironment = [String: String]

public struct Size {
  let width: Double
  let height: Double
}

extension String: Error {}

@available(macOS 10.13, *)
public struct ShellCommand {
  public let name: String
  public let arguments: ShellAguments
  public let environment: ShellEnvironment?
  public let emoji: String?
  public let echoString: String?
  public let directory: String

  public static var defaultEmoji = "🦄"

  public init(named name: String, arguments: ShellAguments = [], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) {
    self.name = name
    self.arguments = arguments
    self.environment = environment
    self.emoji = emoji
    self.echoString = echoString
    self.directory = directory
  }
}

@available(macOS 10.13, *)
extension ShellCommand {
  public static func shell(named name: String, _ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: name, arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func echo(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "echo", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func rm(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "rm", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func copy(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "cp", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func git(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "git", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func swift(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "swift", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func swiftformat(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "swiftformat", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func swiftlint(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "swiftlint", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func sourcekitten(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "sourcekitten", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func jazzy(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "jazzy", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func dockerCompose(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "docker-compose", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func transport(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "/usr/local/itms/bin/iTMSTransporter", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func xcodegen(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "xcodegen", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func xcodebuild(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "xcodebuild", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func curl(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "curl", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func image(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "convert", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }

  public static func agvtool(_ arguments: [String], environment: ShellEnvironment? = nil, emoji: String? = ShellCommand.defaultEmoji, echoString: String? = nil, directory: String = Shell.cwd) -> ShellCommand {
    return ShellCommand(named: "agvtool", arguments: arguments, environment: environment, emoji: emoji, echoString: echoString, directory: directory)
  }
}
