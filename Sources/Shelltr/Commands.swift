//
//  Commands.swift
//  Shelltr
//
//  Created by Cavelle Benjamin on 19-Feb-17 (07).
//

import Foundation

@available(macOS 10.13, *)
public func echo(_ words: @autoclosure () -> String) throws {
  try Shell(.bash).execute(.echo([words()]), reportOut: false).printOutput()
}
