#!/usr/bin/swift sh

import Foundation
import Shelltr // https://gitlab.com/thecb4/Shelltr == 0.2.0
import ChangeLoggerKit // https://gitlab.com/thecb4/changelogger == 0.2.0

@available(macOS 10.13, *)
extension Shell {
  public static func _swifttest(shellType: ShellType = .bash, arguments: [String] = []) throws {
    let args = ["test"] + arguments
    try Shell(shellType).execute(.swift(args))
  }
}

if #available(macOS 10.13, *) {
  Shell.work {
    // tell me what you are changing
    try Shell.assertModified(file: "commit.yaml")

    // get rid of all the old data
    try Shell.rm(content: ".build", recursive: true, force: true)

    // Lint & Format
    try Shell.swiftformat()
    try Shell.swiftlint(quiet: false)

    // delete data and build for docs

    try Shell.swifttest()
    try Shell.sourcekitten(module: "Shelltr", destination: Shell.cwd + "/docs/source.json")
    try Shell.jazzy()

    // cleanup
    try Shell.rm(content: ".build", recursive: true, force: true)

    // add + commit
    try ChangeLog.revise()
    try Shell.gitAdd(.all)
    let message = try CommitEntry.current().summary
    try Shell.gitCommit(message: message)
  }
}
