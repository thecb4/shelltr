# Shelltr Change Log

All notable changes to this project will be documented in this file.

* Format based on [Keep A Change Log](https://keepachangelog.com/en/1.0.0/)
* This project adheres to [Semantic Versioning](http://semver.org/).


## [unreleased] - 2019-Mar-10 20:41:59.
### Added
-

### Changed
- bumped ChangeLogger to 0.2.0 in workflow

### Deprecated
-

### Removed
-

### Fixed
-

### Security
-



